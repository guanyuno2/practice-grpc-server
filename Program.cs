using practice_grpc_server.Protos.Services;
using practice_grpc_server.Protos.Shared;
using practice_grpc_server.Protos.Shared.Interfaces;
using ProtoBuf.Grpc.Reflection;
using ProtoBuf.Grpc.Server;
using ProtoBuf.Meta;

var generator = new SchemaGenerator
{
    ProtoSyntax = ProtoSyntax.Proto3
};

var schema = generator.GetSchema<ICouponService>(); // there is also a non-generic overload that takes Type

using (var writer = new System.IO.StreamWriter("./Grpc/Protos/gofirst.proto"))
{
    await writer.WriteAsync(schema);
}


var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddCodeFirstGrpc();
builder.Services.AddGrpc();
var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

var summaries = new[]
{
    "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
};

app.MapGet("/weatherforecast", () =>
    {
        var forecast = Enumerable.Range(1, 5).Select(index =>
                new WeatherForecast
                (
                    DateOnly.FromDateTime(DateTime.Now.AddDays(index)),
                    Random.Shared.Next(-20, 55),
                    summaries[Random.Shared.Next(summaries.Length)]
                ))
            .ToArray();
        return forecast;
    })
    .WithName("GetWeatherForecast")
    .WithOpenApi();

app.MapGrpcService<CouponService>();




app.Run();

record WeatherForecast(DateOnly Date, int TemperatureC, string? Summary)
{
    public int TemperatureF => 32 + (int)(TemperatureC / 0.5556);
}