using practice_grpc_server.Protos.Shared;
using practice_grpc_server.Protos.Shared.Interfaces;
using ProtoBuf.Grpc;

namespace practice_grpc_server.Protos.Services;

public class CouponService : ICouponService
{
    public Task<CouponReply> ReserveCoupon(CouponRequest request, CallContext context = default)
    {
        return Task.FromResult(new CouponReply()
        {
            Data = new AccountTransaction()
            {
                ParentAccountTransactionId = "tyvan-abc",
                AccountTransactionId = "asd-sdksjd"
            },
            Error = false
        });
    }
}