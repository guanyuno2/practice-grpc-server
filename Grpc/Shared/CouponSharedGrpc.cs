using ProtoBuf.Grpc;
using System.Runtime.Serialization;
using System.ServiceModel;

namespace practice_grpc_server.Protos.Shared;

[DataContract]
public class CouponReply
{
    [DataMember(Order = 1, Name = "data")] public AccountTransaction Data { get; set; }
    [DataMember(Order = 2, Name = "error")] public bool? Error { get; set; }
    [DataMember(Order = 3, Name = "error_code")] public string? ErrorCode { get; set; }
    [DataMember(Order = 4, Name = "error_message")] public string? ErroMessage { get; set; }
}

[DataContract]
public class AccountTransaction
{
    [DataMember(Order = 1, Name = "account_transaction_id")] public string? AccountTransactionId { get; set; }
    [DataMember(Order = 2, Name = "parent_account_transaction_id")] public string? ParentAccountTransactionId { get; set; }
}

[DataContract]
public class CouponRequest
{
    [DataMember(Order = 1, Name = "coupon_token")] public string CouponToken { get; set; }
}
