using System.ServiceModel;
using ProtoBuf.Grpc;

namespace practice_grpc_server.Protos.Shared.Interfaces;

[ServiceContract]
public interface ICouponService
{
    [OperationContract]
    Task<CouponReply> ReserveCoupon(CouponRequest request,
        CallContext context = default);
}